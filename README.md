# Higher-Dimensional Data Fitting Genetic Algorithm (GA)
## Project Goals
The goal of the project is to take in a csv file containing a dataset of triplets of points (x, y, z) and to find a polynomial f(x, y) that will fit the given z data with a particular regression coefficient threshold.
## Files (important files bolded)
*  **main.cpp**: where the GA is located
*  **include/genetic.h**: functions for the GA
*  **include/support.h**: misc. functions like for reading data
*  **PARAMS.txt**: where the user provides the GA with initial conditions
*  **data.csv**: the dataset of points (x, y, z) (note format is strict)
*  **sampleResults.png**: a screenshot of the terminal output after the code runs, initial conditions shown as well
*  plan.txt: misc. thoughts from the very start of the project
*  main.jl: [WIP]
*  dataFunctions.jl: [WIP]
*  functionData.csv: [WIP]