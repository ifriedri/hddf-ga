# for now, assume a function of two variables: z = f(x,y)
include("dataFunctions.jl")
using Distributions, SharedArrays, Plots

# import the data
data = readtable("functionData.csv", header=false)
zData = data[:,1]
xData = data[:,2]
yData = data[:,3]

# set up the population
numIndVars = 2
maxPower = 3
popSize = 10000
cMin = -20
cMax = 20
scores = SharedArray{Float64}(1, popSize)
tmpCoeffs = rand(Uniform(cMin, cMax), 1 + numIndVars*maxPower, popSize)
coeffs = convert(SharedArray, tmpCoeffs)

# evaluate the initial population
evaluatePop(coeffs, scores, data)

# sample from the population
# if score < quantiles[1], best 10%
# if score > quantiles[2], worst 30%
scatter(zeros(size(scores,1)), scores)
sampleSize = convert(Int64, 0.25 * popSize)
sampleIndices = rand(1:10, sampleSize)
sampleScores = SharedArray{Float64}(sampleSize,1)
for i = 1:sampleSize
    sampleScores[i] = scores[sampleIndices[i]]
end
# quantiles = quantile.(
#     Normal(mean(sampleScores), std(sampleScores)),
#     [0.1,0.7]
#     )
# quantiles = quantile.(
#     Normal(mean(scores), std(scores)),
#     [0.1,0.7]
#     )
mu = mean(sampleScores)
sigma = std(sampleScores)
# top10cartInd = findall(tmp -> tmp < quantiles[1], scores)
# bottom30cartInd = findall(tmp -> tmp > quantiles[2], scores)
top10cartInd = findall(tmp -> tmp < (mu - sigma), scores)
bottom30cartInd = findall(tmp -> tmp > (mu + sigma), scores)
top10ind = SharedArray{Int64}(size(top10cartInd,1), 1)
bottom30ind = SharedArray{Int64}(size(bottom30cartInd,1), 1)
for i = 1:size(top10cartInd,1)
    top10ind[i] = top10cartInd[i][2]
end
for i = 1:size(bottom30cartInd,1)
    bottom30ind[i] = bottom30cartInd[i][2]
end
