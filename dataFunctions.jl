# this file is meant to generate a CSV to be read in by the program

using Random, Plots, DataFrames, CSV

function actualFun(x, y)
    return 3 + 2*x + 15*x^2 - 4*x^3 + 8*y - 10*y^2 +2*y^3
end

function g3(numPoints)
    # do x and y from 0 to 10
    #xlist = Array{Float64}(0)
    #ylist = Array{Float64}(0)
    #zlist = Array{Float64}(0)

    for i = 1:numPoints
        x = rand() * 10
        y = rand() * 10
        z = actualFun(x, y)

        io = open("functionData.csv", "a")
        write(io, string(z, ",", x, ",", y, "\n"))
        close(io)
    end
end

function plotData()
    data = readtable("functionData.csv", header=false)
    z = data[:,1]
    x = data[:,2]
    y = data[:,3]
    scatter(x, y, z)
end

function rc(a)
    return rand()*a
end

function evaluatePop(coeffs, scores, data)
    zData = data[:,1]
    xData = data[:,2]
    yData = data[:,3]
    popSize = size(scores,2)

    # calculate error
    for i = 1:popSize
        sumOfDifferences = 0
        for j = 1:size(zData,1)
            x = xData[j]
            y = yData[j]
            z = zData[j]
            evaluatedTerms = coeffs[:,i] .* [1, x, x^2, x^3, y, y^2, y^3]
            predictedZ = reduce(+, evaluatedTerms)
            sumOfDifferences += abs(z - predictedZ)
        end
        meanAbsoluteError = sumOfDifferences / size(zData,1)
        scores[i] = meanAbsoluteError
    end

end
