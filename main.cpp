// write the code in c++ first, then convert to julia
// g++ main.cpp -o main

#include "include/support.h"
#include "include/genetic.h"

#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>

#define vf std::vector<float>
#define vvf std::vector<std::vector<float>>

 int main() {
     std::cout << "<<MAIN>> Begin" << std::endl;

     int POPSIZE;
     int DEG; // highest degree of polynomial 
     std::string DATAFILE;
     float RSQR;
     get_initial_conditions(POPSIZE, DEG, DATAFILE, RSQR);
     
     vvf coeffs(POPSIZE);
     vf scores(POPSIZE);
     vvf data = read_data(DATAFILE);
     print_data(data);

    initialize_population(data, coeffs, DEG);
    scores = get_mae_all(data, coeffs);    
    float bestr2 = 0.0;
    while (bestr2 < RSQR) {
        vf quartiles = get_mae_quartiles(scores);
        crossover_all(coeffs, scores, quartiles);
        mutate_all(coeffs, scores, quartiles);
        // at the end of each step calculate r^2 for the coeffVec with the lowest mae
        bestr2 = rsquared_best(data, coeffs, scores);
        std::cout << bestr2 << std::endl;
        scores = get_mae_all(data, coeffs);
    } 
 }