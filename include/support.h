#ifndef SUPPORT_H
#define SUPPORT_H

// miscellaneous support functions

#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#define vf std::vector<float>
#define vvf std::vector<std::vector<float>>

// -------------------------------------------------------------------------------------------

std::string grab_from_params(std::string line) {
    // split by space, grab 2nd part
    std::vector<std::string> splitup;
    std::istringstream iss(line);
    for(std::string s; iss >> s; ) {
        splitup.push_back(s);
    }
    return splitup.at(1); // the value next to the name
}
extern bool get_initial_conditions(int &popsize, int &deg, 
std::string &filename, float &rsquared)
{
    std::ifstream params;
    params.open("PARAMS.txt");
    std::string line;
    if (params.is_open()) {
        while( getline(params, line) ) {
            if (line.find("POPSIZE") != std::string::npos) {
                popsize = std::stoi( grab_from_params(line) );
            } else if (line.find("DEGREE") != std::string::npos) {
                deg = std::stoi( grab_from_params(line) );
            } else if (line.find("FILENAME") != std::string::npos) {
                filename = grab_from_params(line);
            }
            else if (line.find("RSQUARED") != std::string::npos) {
                rsquared = std::stof(grab_from_params(line));
            }
        }
        params.close();
        return true;
    } else {
        std::cout << "<<ERROR>> [get_initial_conditions] file DNE" << std::endl;
        return false;
    }
}

// ---------------------------------------------------------------------------

void print_data(vvf &data) {
    std::cout << "[print_data] ---start---" << std::endl;
    for (int i = 0; i < data.size(); i++) {
        std::cout << data[0][i] << " " << data[1][i] << " " << data[2][i] << std::endl;
    }
    std::cout << "[print_data] ---end---" << std::endl;
}

vvf read_data(std::string filename) {
    std::ifstream data(filename);
    std::string line;
    std::vector<std::vector<std::string> > csvByRows;
    while( std::getline(data,line) ) {
        std::stringstream lineStream(line);
        std::string cell;
        std::vector<std::string> parsedRow;
        while( std::getline(lineStream,cell,',') ) {
            parsedRow.push_back(cell);
        }

        csvByRows.push_back(parsedRow);
    }
    std::vector<std::vector<float>> csvByCols(3);
    for( auto &i : csvByRows) {
        for ( int j=0; j < 3; j++) {
            csvByCols[j].push_back(std::stof(i[j]));
        }
    }
    return csvByCols;
}

#endif