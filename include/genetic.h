#ifndef GENETIC_H
#define GENETIC_H

// functions concerning the genetic algorithm
#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <random>

#include "support.h"

#define vf std::vector<float>
#define vvf std::vector<std::vector<float>>

void print_coeffs(vvf coeffs) {
    std::cout << "[print_coeffs] ---start---" << std::endl;
    for (vf i : coeffs) {
        for (float j : i) {
            std::cout << j << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "[print_coeffs] ---end---" << std::endl;
}

// crude for now, but always correct
vf determine_coeff_range(vvf &data) {
    float max_x = *std::max_element(data[0].begin(), data[0].end());
    float max_y = *std::max_element(data[1].begin(), data[1].end());
    float max_z = *std::max_element(data[2].begin(), data[2].end());
    float min_x = *std::min_element(data[0].begin(), data[0].end());
    float min_y = *std::min_element(data[1].begin(), data[1].end());
    float min_z = *std::min_element(data[2].begin(), data[2].end());
    vf mins = {min_x, min_y, min_z};
    vf maxs = {max_x, max_y, max_z};
    float min = *std::min_element(mins.begin(), mins.end());
    float max = *std::max_element(maxs.begin(), maxs.end());

    if (abs(max) > abs(min)) {
        return {-1*max, max};
    } else {
        return {-1*min, min};
    }
}

// randomly initialize the population
void initialize_population(vvf &data, vvf &coeffs, int DEG) {
    vf tmp = determine_coeff_range(data);
    float min = tmp[0];
    float max = tmp[1];

    std::cout << "[initialize_population] " << "min: " << min << " max: " << max << std::endl;
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(min, max);

    // create coeffs fully
    for (vf& i : coeffs) {
        for (int j=0; j < (2*DEG) + 1; j++) {
            i.push_back(0.0);
        }
    }
    // assign random values
    for (vf& i : coeffs) {
        for (float& j : i) {
            j = dis(gen);
        }
    }
}

// evaluate a 3D polynomial given a single (x,y) and the coeffs
float evaluate(vf &inputs, vf &coeffsVec) {
    int DEG = (coeffsVec.size() - 1) / 2;
    float x = inputs[0];
    float y = inputs[1];
    
    float val = 0.0;
    val += coeffsVec[0];
    for (int i=1; i < DEG+1; i++) {
        val += coeffsVec[i] * pow(x, i);
    }
    for (int i=DEG; i < coeffsVec.size() - 1; i++) {
        int power = i - DEG + 1;
        val += coeffsVec[i+1] * pow(y, power);
    }

    return val;
}

// calculate the mean absolute error of a single polynomial
float get_mae_single(vvf &dataXYZ, vf &coeffsVec) {
    float absSum = 0.0;
    for (int i = 0; i < dataXYZ[0].size(); i++) {
        vf inputs = { dataXYZ[0][i], dataXYZ[1][i] };
        float predicted = evaluate(inputs, coeffsVec);
        float observed = dataXYZ[2][i];
        absSum += abs(observed - predicted);
    }

    return absSum / dataXYZ[0].size();
}

// calculate mae for every member of the population
vf get_mae_all(vvf &dataXYZ, vvf &coeffs) {
    vf ret;
    for (vf i : coeffs) {
        ret.push_back(get_mae_single(dataXYZ, i));
    }
    return ret;
}

// sample from scores to find the quartiles for the scores
vf get_mae_quartiles(vf &scores) {
    int sampleSize = (int) (0.5 * scores.size());
    std::cout << "sampleSize " << sampleSize << std::endl;
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, scores.size());

    std::vector<int> sampleInds;
    for (int i=0; i < sampleSize; i++) {
        sampleInds.push_back(dis(gen));
    }

    vf sample;
    for (int i : sampleInds) {
        sample.push_back(scores[i]);
    }

    std::sort(sample.begin(), sample.end(), std::greater<float>()); // descending, so highest->lowest = worst->greatest
    int q2 = sample.size() / 2;
    int q1 = q2 / 2;
    int q3 = q1 + q2;

    std::cout << "[get_mae_quartiles] " << sample[q1] << " " << sample[q2] << " " << sample[q3] << std::endl;
    return { sample[q1], sample[q2], sample[q3] };
}

// influence the genes of a bad solution with a better solution
// modifies vf bad
// factor should be greater than 1
void crossover(vf &good, vf &bad, float factor) {
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, good.size());

    int numGenesToCross = dis(gen);
    std::vector<int> crossInds;
    for (int i=0; i < numGenesToCross; i++) {
        float ind = dis(gen);
        // do a weighted average (kinda)
        bad[ind] = ((factor * good[ind]) + bad[ind]) / 2;
    }
}

void crossover_all(vvf &coeffs, vf &scores, vf &quartiles) {
    // first, put the scores into quartiles
    std::vector<int> origScoreIndsQ1;
    std::vector<int> origScoreIndsQ2;
    std::vector<int> origScoreIndsQ3;
    std::vector<int> origScoreIndsQ4;
    vf scoresQ1;
    vf scoresQ2;
    vf scoresQ3;
    vf scoresQ4;

    for (int i=0; i < scores.size(); i++) {
        if (scores[i] > quartiles[0]) {
            origScoreIndsQ1.push_back(i);
            scoresQ1.push_back(scores[i]);
        } else if (scores[i] > quartiles[1]) {
            origScoreIndsQ2.push_back(i);
            scoresQ2.push_back(scores[i]);
        } else if (scores[i] > quartiles[2]) {
            origScoreIndsQ3.push_back(i);
            scoresQ3.push_back(scores[i]);
        } else {
            origScoreIndsQ4.push_back(i);
            scoresQ4.push_back(scores[i]);
        } 
    }

    //std::cout << "[crossover_all] sizes: " << scoresQ1.size() << " " << scoresQ2.size() <<  " " << scoresQ3.size() << " " << scoresQ4.size() << std::endl;
    
    for (vf i : coeffs) {
        if (i.size() != 7) std::cout << "false" << std::endl;
    }

    // we want the crossover to make use of Q4
    // pick a random solution from Q4 each time
    // modify Q3, Q2, Q1 in order of increasing agressiveness
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, scoresQ4.size() - 1);
    // modify Q3
    for (int i=0; i < scoresQ3.size(); i++) {
        int randQ4ind = dis(gen);
        crossover(coeffs[origScoreIndsQ4[randQ4ind]], coeffs[origScoreIndsQ3[i]], 1.0);
    }
    // modify Q2
    for (int i=0; i < scoresQ2.size(); i++) {
        int randQ4ind = dis(gen);
        crossover(coeffs[origScoreIndsQ4[randQ4ind]], coeffs[origScoreIndsQ2[i]], 1.25);
    }
    // modify Q1
    for (int i=0; i < scoresQ1.size(); i++) {
        int randQ4ind = dis(gen);
        crossover(coeffs[origScoreIndsQ4[randQ4ind]], coeffs[origScoreIndsQ1[i]], 1.5);
    }
}

// mutate a single solution
// modifies genes
// factor should be less than 1
    // a smaller factor is less agressive
void mutate_genes(vf &genes, int numGenesToMutate, float factor) {
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, genes.size() - 1);

    std::random_device rd2; 
    std::mt19937 gen2(rd());
    std::uniform_real_distribution<> dis2(-1.0*factor, factor);

    for (int i=0; i < numGenesToMutate; i++) {
        genes[dis(gen)] = genes[dis(gen)] + (dis2(gen2) * genes[dis(gen)]);
    }
}

void mutate_all(vvf &coeffs, vf &scores, vf &quartiles) {
    // first, put the scores into quartiles
    std::vector<int> origScoreIndsQ1;
    std::vector<int> origScoreIndsQ2;
    std::vector<int> origScoreIndsQ3;
    std::vector<int> origScoreIndsQ4;
    vf scoresQ1;
    vf scoresQ2;
    vf scoresQ3;
    vf scoresQ4;

    for (int i=0; i < scores.size(); i++) {
        if (scores[i] > quartiles[0]) {
            origScoreIndsQ1.push_back(i);
            scoresQ1.push_back(scores[i]);
        } else if (scores[i] > quartiles[1]) {
            origScoreIndsQ2.push_back(i);
            scoresQ2.push_back(scores[i]);
        } else if (scores[i] > quartiles[2]) {
            origScoreIndsQ3.push_back(i);
            scoresQ3.push_back(scores[i]);
        } else {
            origScoreIndsQ4.push_back(i);
            scoresQ4.push_back(scores[i]);
        } 
    }

    //std::cout << "[mutate_all_genes] sizes: " << scoresQ1.size() << " " << scoresQ2.size() <<  " " << scoresQ3.size() << " " << scoresQ4.size() << std::endl;
    
    // we want the mutations to barely affect Q4
    // modify Q4, Q3, Q2, Q1 in order of increasing agressiveness
    // modify Q4
    for (int i=0; i < scoresQ4.size(); i++) {
        mutate_genes(coeffs[origScoreIndsQ4[i]], 1, 0.005);
    }
    // modify Q3
    for (int i=0; i < scoresQ3.size(); i++) {
        mutate_genes(coeffs[origScoreIndsQ3[i]], 1, 0.1);
    }
    // modify Q2
    for (int i=0; i < scoresQ2.size(); i++) {
        mutate_genes(coeffs[origScoreIndsQ2[i]], 2, 0.2);
    }
    // modify Q1
    for (int i=0; i < scoresQ1.size(); i++) {
        mutate_genes(coeffs[origScoreIndsQ1[i]], 3, 0.3);
    }

}

float rsquared(vvf &data, vf &coeffsVec) {
    float mean = std::accumulate( data[2].begin(), data[2].end(), 0.0) / data[2].size();
    float sstot = 0.0;
    float ssres;
    for (int i = 0; i < data[0].size(); i++) {
        vf inputs = { data[0][i], data[1][i] };
        float predicted = evaluate(inputs, coeffsVec);
        float observed = data[2][i];
        sstot += (observed - mean) * (observed - mean);
        ssres += (observed - predicted) * (observed - predicted);
    }
    return 1 - (ssres / sstot);
}

float rsquared_best(vvf &data, vvf &coeffs, vf &scores) {
    // find the index of the lowest score
    int index = std::distance(scores.begin(), std::min_element(scores.begin(), scores.end()));
    vf a = coeffs[index];
    std::cout << "solution: " << a[0] << " + " << a[1] << "x + " << a[2] << "(x^2) + " << a[3] << "(x^3) + " << a[4] << "y + " << a[5] << "(y^2) + " << a[6] <<"(y^3)" << std::endl; 
    return rsquared(data, a);
}

#endif
